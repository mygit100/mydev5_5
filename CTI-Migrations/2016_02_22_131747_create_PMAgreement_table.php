<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePMAgreementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pmagreement', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->unsigned();
            $table->foreign('property_id')->references('id')->on('propertyaddress');           
            $table->integer('landlord_id')->unsigned();
            $table->foreign('landlord_id')->references('id')->on('landlord');
            $table->integer('agent_id')->unsigned();
            $table->foreign('agent_id')->references('id')->on('agent');
            $table->dateTime('pma_end');
            $table->integer('maintenance_res');
            $table->integer('management_fee');  // % percent
            $table->integer('non_management_fee');  // per hour cost
            $table->integer('sale_commission');  // % percent
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pmagreement');
    }
}
